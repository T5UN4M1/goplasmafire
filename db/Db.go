package db

import (
	"GoPlasmaFire/constants"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

var DB *mongo.Client

var Collections map[string]*mongo.Collection

const (
	PLAYER = "player"
	GALAXY = "galaxy"
)

func DbConnect() {
	clientOptions := options.Client().ApplyURI("mongodb://" +
		constants.DB_CONFIG.Username + ":" +
		constants.DB_CONFIG.Password + "@" +
		constants.DB_CONFIG.Host + ":" +
		constants.DB_CONFIG.Port + "/" +
		constants.DB_CONFIG.Database)
	client, err := mongo.NewClient(clientOptions)
	if err != nil {
		log.Fatal(err)
	}
	err = client.Connect(context.Background())
	if err != nil {
		log.Fatal(err)
	}
	DB = client

	fmt.Println("+++++++++++++++ Connected to MongoDB")

	collectionsList := []string{PLAYER, GALAXY}

	Collections = make(map[string]*mongo.Collection)

	for _, collection := range collectionsList {
		Collections[collection] = DB.Database("plasma").Collection(collection)
		err = Collections[collection].Drop(context.TODO())
		if err != nil {
			log.Fatal(err)
		}
	}

	fmt.Println("+++++++++++++++ Collections emptied")

}
