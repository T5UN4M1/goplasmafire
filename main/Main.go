package main

import (
	"GoPlasmaFire/auth"
	"GoPlasmaFire/db"
	"github.com/gin-gonic/gin"
)

func main() {
	server := gin.Default()

	db.DbConnect()

	server.POST("/api/auth/login", auth.Connect)
	server.POST("/api/auth/register", auth.Register)

	err := server.Run(":1337")
	if err != nil {

	}
}
