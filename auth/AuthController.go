package auth

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
)

type ConnectForm struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}
type RegisterForm struct {
	Username string `json:"username"`
	Mail     string `json:"mail"`
	Password string `json:"password"`
}

func Connect(c *gin.Context) {
	value, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Println(err)
	}
	var connectForm ConnectForm
	err = json.Unmarshal(value, &connectForm)
	if err != nil {
		fmt.Println(err)
	}
	c.JSON(200, gin.H{
		"response": "ok",
	})
}

func Register(c *gin.Context) {
	value, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		fmt.Println(err)
	}
	var registerForm RegisterForm
	err = json.Unmarshal(value, &registerForm)
	if err != nil {
		fmt.Println(err)
	}
	register(&registerForm)
}
