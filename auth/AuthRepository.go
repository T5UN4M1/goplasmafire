package auth

import (
	"GoPlasmaFire/db"
	"GoPlasmaFire/schema"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func countPlayerByUsernameOrMail(username string, mail string) (int64, error) {
	return db.Collections[db.PLAYER].CountDocuments(context.TODO(), bson.M{"$or": []interface{}{
		bson.M{username: username},
		bson.M{mail: mail},
	}})
}
func insertPlayer(player *schema.Player) (*mongo.InsertOneResult, error) {
	return db.Collections[db.PLAYER].InsertOne(context.TODO(), *player)
}
