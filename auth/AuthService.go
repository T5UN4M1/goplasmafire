package auth

import (
	"GoPlasmaFire/schema"
	"golang.org/x/crypto/bcrypt"
)

func register(registerForm *RegisterForm) error {
	c, err := countPlayerByUsernameOrMail(registerForm.Username, registerForm.Password)
	println("result" + string(c))
	if int(c) != 0 || err != nil {
		return err
	}
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(registerForm.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	player := schema.Player{
		Username: registerForm.Username,
		Mail:     registerForm.Mail,
		Password: hashedPassword,
	}
	_, err = insertPlayer(&player)
	return err
}
