package schema

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Player struct {
	Id       primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Username string             `json:"username" bson:"username"`
	Mail     string             `json:"mail" bson:"mail"`
	Password []byte             `json:"password" bson:"password"`
}
